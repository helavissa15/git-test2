import { Component, OnInit } from '@angular/core';
import {FormArray, FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-foi-form2',
  templateUrl: './foi-form2.component.html',
  styleUrls: ['./foi-form2.component.scss']
})
export class FoiForm2Component implements OnInit {

  listForm = new FormGroup({
    startDate: new FormControl(''),
    endDate: new FormControl('')
  });

  fois = new FormGroup({});
  testInput = 'test input';

  constructor() {}

  getDay(day: string): FormArray {
    return this.fois.get(day) as FormArray;
  }

  getFoiControls(): string[]{
    return Object.keys(this.fois.controls);
  }

  ngOnInit(): void {

    this.fois.addControl('0', new FormArray([
      new FormControl('01-01-2021'),
      new FormControl('101'),
      new FormControl('201')
    ]));
    this.fois.addControl('1', new FormArray([
      new FormControl('02-01-2021'),
      new FormControl('102'),
      new FormControl('202')
    ]));
    this.fois.addControl('2', new FormArray([
      new FormControl('02-01-2021'),
      new FormControl('103'),
      new FormControl('203')
    ]));

  }

  submitForm(): void {
    console.log(this.fois.value);
  }


}


