import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoiForm2Component } from './foi-form2.component';

describe('FoiForm2Component', () => {
  let component: FoiForm2Component;
  let fixture: ComponentFixture<FoiForm2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoiForm2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoiForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
