import { Component, OnInit } from '@angular/core';
import {Form, FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment/moment';
import {BehaviorSubject, forkJoin, interval, Observable, of, ReplaySubject, Subject} from 'rxjs';
import {delay, first, map, scan, take} from 'rxjs/operators';
import {fromArray} from 'rxjs/internal/observable/fromArray';
import {TranslationService} from '../services/translation.service';
import {isElementScrolledOutsideView} from '@angular/cdk/overlay/position/scroll-clip';


@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {


  constructor(private translationService: TranslationService) {
  }

  ngOnInit(): void {
    // this.translationService.getData().subscribe(data => {
    //   console.log('got data', data);
    // });


    this.translationService.getRefresh().pipe(scan(val => !val, true)).subscribe(value => {
      console.log(value);
      this.translationService.updateRefresh();
    });
  }

  updateRefresh() {
    this.translationService.updateRefresh();
  }

  getRefresh() {
    this.translationService.getRefresh().pipe(first()).subscribe(value => {
      console.log(value);
      this.translationService.updateRefresh();
    });
  }

  test() {
    this.translationService.getRefresh().pipe(scan(val => !val, false)).subscribe(value => {
      console.log(value);
    });
  }
}
