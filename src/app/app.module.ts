import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeroFormComponent } from './hero-form/hero-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import { TestComponent } from './test/test.component';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatNativeDateModule} from '@angular/material/core';
import {MatMomentDateModule, MomentDateAdapter} from '@angular/material-moment-adapter';
import { TestFormComponent } from './test-form/test-form.component';
import { FoiFormComponent } from './foi-form/foi-form.component';
import { FoiForm2Component } from './foi-form2/foi-form2.component';
import { InputOnClickDirective } from './directives/input-on-click.directive';
import { InputOnClickComponent } from './components/input-on-click/input-on-click.component';
import { UpperLowerDirective } from './directives/upperLower/upper-lower.directive';
import {Observable} from 'rxjs';
import {TranslationService} from './services/translation.service';

export const CUSTOM_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

function initializeApp(): Promise<any> {
  return new Promise((resolve, reject) => {

    setTimeout(() => {
      resolve({cat: 'Frida', dog: 'Wof wof'});

        }, 1000);
    });
}


@NgModule({
  declarations: [
    AppComponent,
    HeroFormComponent,
    TestComponent,
    TestFormComponent,
    FoiFormComponent,
    FoiForm2Component,
    InputOnClickDirective,
    InputOnClickComponent,
    UpperLowerDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatInputModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatFormFieldModule,
    MatSelectModule,
    NoopAnimationsModule,
    ReactiveFormsModule
  ],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: CUSTOM_FORMATS},
    // {
    //   provide: APP_INITIALIZER,
    //   useFactory: () => initializeApp,
    //   multi: true
    // }
    {
      provide: APP_INITIALIZER,
      useFactory: (ts: TranslationService) => () => ts.loadTranslations(),
      deps: [TranslationService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
