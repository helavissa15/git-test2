import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TestFormComponent} from './test-form/test-form.component';
import {TestComponent} from './test/test.component';

const routes: Routes = [
  {path: '', component: TestComponent},
  {path: 'test-form', component: TestFormComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
