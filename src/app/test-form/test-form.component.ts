import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TranslationService} from '../services/translation.service';

@Component({
  selector: 'app-test-form',
  templateUrl: './test-form.component.html',
  styleUrls: ['./test-form.component.scss']
})
export class TestFormComponent implements OnInit {

  constructor(private translationService: TranslationService) {

  }

  ngOnInit(): void {
    this.translationService.getData().subscribe(data => {
      console.log('got data', data);
    });
  }


}
