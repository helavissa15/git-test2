import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, ReplaySubject} from 'rxjs';
import {filter} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  translations;
  replaySubject: ReplaySubject<any>;
  counter = 0;
  refresh = new BehaviorSubject(false);

  constructor() {
    this.replaySubject = new ReplaySubject<any>(1);
    // console.log('translation service contructor');
  }

  loadTranslations(): Promise<any> {

    if (!this.translations) {
      return new Promise((resolve, reject) => {

        setTimeout(() => {
          resolve({foo: 'FOO', bar: 'BAR'});
        }, 1000);

      }).then(data => {
        this.translations = data;
        this.replaySubject.next(123);
        // console.log('replay subj next');
      });
    }
  }

  getData(): Observable<any> {
    return this.replaySubject.asObservable();
  }

  updateRefresh() {
    // this.refresh.next(++this.counter);
    this.refresh.next(true);
  }

  getRefresh(): Observable<any> {
    return this.refresh.pipe(filter(
       result => {
         return result !== null;
       }
     ));
    // return this.refresh.asObservable();
  }

  getValue() {
    return this.refresh.value;
  }

}
