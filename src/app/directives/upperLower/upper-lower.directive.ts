import {Directive, ElementRef, HostListener, Renderer2} from '@angular/core';
import {NgControl} from '@angular/forms';

@Directive({
  selector: '[appUpperLower]'
})
export class UpperLowerDirective {

  get ctrl() {
    return this.ngControl.control;
  }

  constructor(
    private ngControl: NgControl,
    private el: ElementRef,
    private renderer2: Renderer2
  ) {
    this.renderer2.setStyle(this.el.nativeElement, 'border', 'none');
  }

  @HostListener('focus')
  onFocus(value: any) {
    // this.ctrl?.setValue(this.ctrl.value.toUpperCase());
    this.renderer2.setStyle(this.el.nativeElement, 'border', null);
  }

  @HostListener('blur')
  onBlur(value: any) {
    // this.ctrl?.setValue(this.ctrl.value.toLowerCase());
    this.renderer2.setStyle(this.el.nativeElement, 'border', 'none');
  }

}
