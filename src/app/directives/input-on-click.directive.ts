import {Directive, ElementRef, HostListener, Input, Renderer2, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appInputOnClick]'
})
export class InputOnClickDirective {

  // constructor(private templateRef: TemplateRef<any>,
  //             private viewContainer: ViewContainerRef) { }

  constructor(private el: ElementRef, private renderer2: Renderer2) {
  }

  @HostListener('focus') onEnter() {
    // this.viewContainer.createEmbeddedView(this.templateRef);
    this.renderer2.setStyle(this.el.nativeElement, 'color', 'blue');
  }

  @HostListener('blur') onLeave() {
    this.renderer2.setStyle(this.el.nativeElement, 'color', null);
  }

}
