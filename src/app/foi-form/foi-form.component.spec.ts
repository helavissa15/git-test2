import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoiFormComponent } from './foi-form.component';

describe('FoiFormComponent', () => {
  let component: FoiFormComponent;
  let fixture: ComponentFixture<FoiFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoiFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoiFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
