import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-foi-form',
  templateUrl: './foi-form.component.html',
  styleUrls: ['./foi-form.component.scss']
})
export class FoiFormComponent implements OnInit {

  listForm = new FormGroup({
    startDate: new FormControl(''),
    endDate: new FormControl('')
  });

  fois = new FormGroup({});
  days = new Array<string>();
  foiNames = ['Seasonality', 'Competitor Spend', 'Other'];

  constructor(private fb: FormBuilder) {}

  getDay(day: string): FormArray {
    return this.fois.get(day) as FormArray;
  }

  getFoiKeys(): string[]{
    // console.log(Object.keys(this.fois.controls));
    return Object.keys(this.fois.controls);
  }

  ngOnInit(): void {}

  dateRangeChange() {
    const startDate = this.listForm.get('startDate')?.value;
    const endDate = this.listForm.get('endDate')?.value;
    if (startDate && endDate){

      const newFois = new FormGroup({});
      this.days = new Array<string>();
      for (const m = startDate.clone(); m.diff(endDate, 'days') <= 0; m.add(1, 'days')) {

        const date = m.format('DD-MM-YYYY');

        this.addFoi(newFois, date);
        this.days.push(date);
      }

      this.fois = newFois;
    }
  }

  addFoi(fois: FormGroup, date: any) {

    fois.addControl(date, this.fb.array(
      this.getFoiValuesForDate(date)
    ));

  }

  getFoiValuesForDate(date: string){
    return this.fois.get(date) ? this.fois.get(date)?.value : ['', '', ''];
  }

  submitForm(): void {
    console.log(this.fois.value);
  }


}

